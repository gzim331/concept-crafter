async function getNote(projectId) {
    const response = await fetch('http://localhost:8000/concept/crafter/note/' + projectId, {
        method: 'GET'
    });

    return response.json();
}

export default getNote
async function postNote(projectId, data = {}) {
    const response = await fetch('http://localhost:8000/concept/crafter/note/' + projectId, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    });

    return response.json();
}

export default postNote
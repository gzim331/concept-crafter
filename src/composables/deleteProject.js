async function deleteProject(projectId) {
    const response = await fetch('http://localhost:8000/concept/crafter/projects/' + projectId, {
        method: 'DELETE'
    });

    return response.json();
}

export default deleteProject
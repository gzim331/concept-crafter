async function postProject(data = {}) {
    const response = await fetch('http://localhost:8000/concept/crafter/projects', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    });

    return response.json();
}

export default postProject
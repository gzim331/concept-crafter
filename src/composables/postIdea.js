async function postIdea(projectId, data = {}) {
    const response = await fetch('http://localhost:8000/concept/crafter/post-idea/' + projectId, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    });

    return response.json();
}

export default postIdea
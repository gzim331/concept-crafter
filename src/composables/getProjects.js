async function getProjects() {
    const response = await fetch('http://localhost:8000/concept/crafter/projects', {
        method: 'GET'
    });

    return response.json();
}

export default getProjects
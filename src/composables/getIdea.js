async function getIdea(projectId) {
    const response = await fetch('http://localhost:8000/concept/crafter/idea/' + projectId, {
        method: 'GET'
    });

    return response.json();
}

export default getIdea
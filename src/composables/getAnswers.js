async function getAnswers(projectId) {
    const response = await fetch('http://localhost:8000/concept/crafter/get-answers/' + projectId, {
        method: 'GET'
    });

    return response.json();
}

export default getAnswers